from django.forms import ModelForm

from . import models


class ExpertForm(ModelForm):
    class Meta:
        model = models.Expert
        fields = ('username',)
