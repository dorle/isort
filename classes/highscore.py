from .models import Expert, Label

class Scores(object):
    """Keeps track of scores.

    Keeps scores in memory so that we don't have to query the database.

    """

    def __init__(self):
        self.counts = dict()
        for e in Expert.objects.all():
            self.counts[e.pk] = e.get_labels().count()

    def increase_count(self, expert):
        try:
            self.counts[expert.pk] += 1
        except KeyError:
            self.counts[expert.pk] = 1

    def decrease_count(self, expert):
        try:
            self.counts[expert.pk] -= 1
        except KeyError:
            self.counts[expert.pk] = 0

    def get(self, pk):
        return self.counts[pk]

    def get_ranking(self):
        pass

    def get_rank(self, pk):
        rank = 1
        my_score = self.get(pk)

        for (key, value) in self.counts.items():
            if key != pk and value > my_score:
                rank += 1

        return rank

    def get_diff_to_next(self, pk):
        ranking = sorted(self.counts.values(), reverse=True)
        if len(ranking) < 2:
            return "-"

        rank = self.get_rank(pk)
        if rank == 1:
            a = ranking[rank-1]
            b = ranking[rank]
        else:
            a = ranking[rank-2]
            b = ranking[rank-1]

        return a-b

    def highscore(self):
        return max(self.counts.values())

# Singleton highscore list.
scores = Scores()
