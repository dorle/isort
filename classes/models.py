from django.db import models
from django.db.models import Max
from django.urls import reverse
from django.contrib.auth.models import User
import random
import timeit


class Image(models.Model):
    file_name = models.CharField(max_length=200)

    def get_absolute_url(self):
        return reverse('image-detail', args=[str(self.id)])

    def get_labels(self):
        u = Image.objects.get(pk=self.id)
        labels = u.label_set.all().values('name')
        return labels

    def has_labels(self):
        if self.get_labels().count() > 0:
            return True
        else:
            return False

    def get_full_path(self):
        return "images/"+self.file_name

    def __str__(self):
        return self.file_name

    queue = []
    queue_limit = 1000

    @classmethod
    def get_next(cls):
        """Returns the next image for classification."""
        try:
            return cls.queue.pop()
        except IndexError:
            # Queue is empty.
            pass

        new_queue = list(get_images()[:cls.queue_limit])
        assert len(new_queue) > 0
        random.shuffle(new_queue)
        i = new_queue.pop()
        cls.queue = new_queue
        return i


def get_unlabeled_images():
    return Image.objects.exclude(label__name="Human").exclude(label__name="Non-Human").exclude(label__name="Undecided")

def get_undecided_images():
    return Image.objects.filter(label__name="Undecided")

def get_all_images():
    return Image.objects.all()

def get_images():
    # XXX: Make this policy configurable here.
    qs = get_unlabeled_images()

    # Now make sure we have at least one.
    if qs.count() == 0:
        qs = get_all_images()
    return qs


def get_unlabeled_image():
    max_id = Image.objects.all().aggregate(max_id=Max("id"))['max_id']
    if max_id is None:
        max_id = 0
    start = timeit.default_timer()
    while timeit.default_timer() - start < 2:
        pk = random.randint(0, max_id)
        img = Image.objects.filter(pk=pk).first()
        if img:
            if not img.has_labels():
                return img

    return "nothing.jpg"

def get_random_image():
    max_id = Image.objects.all().aggregate(max_id=Max("id"))['max_id']
    if max_id is None:
        return "nothing.jpg"
    start = timeit.default_timer()
    while timeit.default_timer() - start < 2:
        pk = random.randint(0, max_id)
        img = Image.objects.filter(pk=pk).first()
        if img:
            return img
    return "nothing.jpg"


class Label(models.Model):
    name = models.CharField(max_length=200)
    image = models.ForeignKey(Image, on_delete=models.CASCADE)
    expert = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    date = models.DateTimeField(auto_now_add=True, blank=True)

    def get_absolute_url(self):
        return reverse('label-detail', args=[str(self.id)])

    def get_images(self):
        u = Label.objects.get(pk=self.id)
        images = u.image_set.all()
        return images

    def __str__(self):
        return self.name


class Expert(User):

    class Meta:
        proxy = True

    def get_labels(self):
        u = Expert.objects.get(pk=self.id)
        labels = u.label_set.all().order_by('-date')
        return labels

    def get_absolute_url(self):
        return reverse('expert-detail', args=[str(self.id)])
